<?php
session_start();
if (isset($_SESSION['user'])) {


  include_once('dbconnection.php');


  if (isset($_GET['update'])) {
?>

    <script>
      alert("Data updated successfully");
    </script>
  <?php
  }
  if (isset($_GET['add'])) {
  ?>
    <script>
      alert("Data inserted successfully");
    </script>
  <?php
  }
  if (isset($_GET['change'])) {
  ?>
    <script>
      alert("Password Changed Successfully");
    </script>

  <?php
  }



  //If no data will be found when searching
  if (isset($_GET['error1'])) {
  ?>
    <div class="alert alert-danger">
      No Such Employee Found
    </div>
  <?php
  }



  if (isset($_GET['page'])) {
    $page = $_GET['page'];
  } else {
    $page = 1;
  }

  $per_page = 05;
  $start = ($page - 1) * $per_page;
  //If there is search entry
  //This will happen after search button will be clicked
  if (isset($_POST['search'])) {
    $field = $_POST['searchfield'];
    $sql = "select * from employeedetails where firstname LIKE '%$field%' OR lastname LIKE '%$field%' OR EmailID LIKE '%$field%'OR salary='$field'OR department LIKE '%$field%'OR gender LIKE '$field%'OR phonenumber LIKE '%$field%'OR employeeid='$field' limit $start,$per_page";
  } else {
    //To show all the records in the database
    //this will happen when add employee button will be clicked
    $sql = "select * from employeedetails limit $start,$per_page";
  }
  $query = mysqli_query($conn, $sql);
  $num = mysqli_num_rows($query);
  // $total_result=mysqli_num_rows($page_result);

  //  $total_page=ceil($num/$per_page);
  if ($num == 0) {

    header('location:welcome2.php?error1=1');
  }

  ?>


  <!DOCTYPE html>
  <html>

  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <style>
      .topnav .search-container {
        float: right;
      }

      .topnav input[type=text] {
        padding: 6px;
        margin-top: 6px;
        font-size: 17px;
        margin-right: 2px;
      }

      .topnav .search-container button {
        float: right;
        padding: 6px 10px;
        margin-top: 8px;
        margin-right: 13px;
        font-size: 17px;
        border: none;
        cursor: pointer;
      }

      .topnav .search-container button:hover {
        background: #ccc;
      }

      .topnav input[type=text] {
        border: 2px solid #ccc;
      }
    </style>




    <!-- the database table -->
  </head>

  <body>
    <div class="topnav">

      <!-- change password button -->

      <!-- search container -->
      <div class="search-container">
        <form action="welcome2.php" method="post">
          <input type="text" placeholder="Search" class="form-group" name="searchfield">
          <button name="search" class="btn btn-success">Search</button>
        </form>
      </div>
    </div>
    <a href="add.php">
      <button name="add" class="btn btn-success">ADD Employee</button>
    </a>
    <div class="container">

      <table class="table table-bordered">
        <thead>
          <tr>
            <th>EmployeeID</th>
            <th>ProfilePicture</th>

            <th>Firstname</th>
            <th>Lastname</th>
            <th>EmailID</th>
            <th>Salary</th>
            <th>Department</th>
            <th>Gender</th>
            <th>PhoneNumber</th>
            <th>Edit</th>
            <th>Delete</th>

          </tr>
        </thead>
        <tbody>
          <?php
          while ($row = mysqli_fetch_assoc($query)) {
          ?>
            <tr>
              <td><?php echo $row['employeeid']; ?></td>
              <td><?php echo '<img  src="data:image;base64,' . base64_encode($row['profilepicture']) . '"  style="width:100px; height:100px ">'; ?></td>

              <td><?php echo $row['firstname']; ?></td>
              <td><?php echo $row['lastname']; ?></td>
              <td><?php echo $row['EmailID']; ?></td>
              <td><?php echo $row['salary']; ?></td>
              <td><?php echo $row['department']; ?></td>
              <td><?php echo $row['gender']; ?></td>
              <td><?php echo $row['phonenumber']; ?></td>
              <!-- <td>
            <?php// echo '<img  src="data:image;base64,'.base64_encode($row['profilepicture']).'"  style="width:100px; height:100px ">'; ?>
            </td> -->

              <td><a href="edit.php?employeeid=<?php echo $row['employeeid']; ?>">
                  <button name="edit" class="btn btn-success">EDIT</button>

                </a></td>
              <!-- <td><button name="edit" class="btn btn-success">EDIT</button></td> -->

              <!-- <td><button name="edit" class="btn btn-success">EDIT</button></a> -->
              <!-- <td><a href="insert.php?delete=<?php echo $row['employeeid']; ?>">
            <button name="delete" class="btn btn-danger">DELETE</button>
            </a></td> -->
              <td><button onClick=deleteconfirm(<?php echo $row['employeeid']; ?>) name="delete" class="btn btn-danger">DELETE</button></td>
              </td>



            </tr>
            <script>
              function deleteconfirm(id) {
                if (confirm("Do you want to Delete")) {
                  window.location.href = 'actions.php?delete=' + id + '';
                  return true;

                }
              }
            </script>
          <?php
          }
          ?>
        </tbody>
      </table>
      <!-- for pagination -->

      <?php







      $page_query = "select * from employeedetails";

      $page_result = mysqli_query($conn, $page_query);

      $total_result = mysqli_num_rows($page_result);

      $total_page = ceil($total_result / $per_page);

      ?>
      <div class="text-center">
        <?php
        if ($page > 1) {
        ?>
          <a href="welcome2.php?page=<?php echo ($page - 1); ?>">
            <button name="" class="btn btn-success">Previous</button>

          </a>
        <?php
        }
        for ($i = 1; $i <= $total_page; $i++) {
        ?>
          <a href="welcome2.php?page=<?php echo $i; ?>">
            <button name="" class="btn btn-primary"><?php echo $i; ?></button>

          </a>
        <?php
        }
        $i--;
        if ($i > $page) {

        ?>

          <a href="welcome2.php?page=<?php echo ($page + 1); ?>">
            <button name="" class="btn btn-success">Next</button>

          </a>
        <?php
        }


        ?>
      </div>









    </div>
    <?php

    if (isset($_POST['search'])) {
    ?>

      <a href="welcome2.php">
        <button name="all" class="btn btn-success">All Records</button>
      </a>
    <?php
    }
    ?>
    <a href="change.php">
      <button name="change" class="btn btn-success">Change Password</button>
    </a>


  </body>




  </html>



<?php
  //logout button

  echo '<a href="logout.php">
    <button name="logout" class="btn btn-success">Logout</button></a>';
} else {
  header('location:main.php');
}



?>