<?php
session_start();
if (isset($_SESSION['user'])) {
  include_once('dbconnection.php');
  $query = "select * from employeedetails";
  $stmt = $con->query($query);
  $rows = $stmt->fetchAll();
?>
  <!-- html starting -->
  <!DOCTYPE html>
  <html>
  <head>
    <title>TablePage</title>
    <!-- including header file for libraries -->
    <?php include_once('header.html'); ?>
    <!-- including javasript file -->
    <script src="../asset/js/sidebar.js"></script>
    <!-- including css file -->
    <link rel="stylesheet" href="../asset/css/styling.css">
    <link rel="stylesheet" href="../asset/css/sidebar.css">
  </head>
  <body>
<div id="mySidebar" class="sidebar">
  <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">×</a>
  <div class="search-container">
        <form action="welcome2.php" method="post">
          <input type="text" placeholder="Search" class="form-group" name="searchfield">
          <button name="search" class="glyphicon glyphicon-search">search</button>
        </form>
      </div>
      <a href="add.php">
        Add New Employee
      </a>
    <?php
    //logout button
    echo '<a href="logout.php">
    Logout</a>';
    ?>
 
</div>

<div id="main">
  <button class="openbtn" onclick="openNav()">☰</button>  
  
</div>

    
    
    <div class="container">
      <!-- the database table -->
      <table class="table table-striped">
        <thead>
          <tr>
            <th>Employee ID</th>
            <th>Profile Picture</th>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Email ID</th>
            <th>Salary</th>
            <th>Department</th>
            <th>Gender</th>
            <th>Phone Number</th>
            <th>Edit</th>
            <th>Delete</th>
          </tr>
        </thead>
        <tbody>
          <?php
          foreach ($rows as $row) {
          ?>
            <tr>
              <td><?php echo $row['employeeid']; ?></td>
              <td><?php echo '<img  src="data:image;base64,' . base64_encode($row['profilepicture']) . '"  style="width:100px; height:100px ">'; ?></td>
              <td><?php echo $row['firstname']; ?></td>
              <td><?php echo $row['lastname']; ?></td>
              <td><?php echo $row['EmailID']; ?></td>
              <td><?php echo $row['salary']; ?></td>
              <td><?php echo $row['department']; ?></td>
              <td><?php echo $row['gender']; ?></td>
              <td><?php echo $row['phonenumber']; ?></td>
              <!-- Edit button -->
              <td><a href="edit.php?employeeid=<?php echo $row['employeeid']; ?>">
                  <button class="btn btn-success btn-sm rounded-0" type="button" data-toggle="tooltip" data-placement="top" name="edit" title="Edit"><i class="fa fa-edit"></i></button>
                </a></td>
              <!-- Delete button -->
              <td> <button onClick=deleteconfirm(<?php echo $row['employeeid']; ?>) name="delete" class="btn btn-danger btn-sm rounded-0" type="button" data-toggle="tooltip" data-placement="top" title="Delete"><i class="fa fa-trash"></i></button></td>
            </tr>
            <!-- funtion for confirmation of delete -->
            <script>
              function deleteconfirm(id) {
                if (confirm("Do you want to Delete")) {
                  window.location.href = 'actions.php?delete=' + id + '';
                  return true;
                }
              }
            </script>
          <?php
          }
          ?>
        </tbody>
      </table>
  </body>

  </html>










<?php



} else {
  header('location:main.php');
}

?>