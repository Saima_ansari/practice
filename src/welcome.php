<?php
session_start();
if (isset($_SESSION['user'])) {
  include_once('dbconnection.php');
  //The message for data updated successfully
  if (isset($_GET['update'])) {
?>
    <script>
      alert("Data updated successfully");
    </script>
  <?php
  }
  //The message for data inserted successfully
  if (isset($_GET['add'])) {
  ?>
    <script>
      alert("Data inserted successfully");
    </script>
  <?php
  }
  //The message for changing the passsword
  if (isset($_GET['change'])) {
  ?>
    <script>
      alert("Password Changed Successfully");
    </script>
  <?php
  }
  //If no data will be found when searching
  if (isset($_GET['error1'])) {
  ?>
    <div class="alert alert-danger">
      No Such Employee Found
    </div>
  <?php
  }
//For pagination
  if (isset($_GET['page'])) {
    $page = $_GET['page'];
  } else {
    $page = 1;
  }
  $per_page = 05;
  $start = ($page - 1) * $per_page;
  //If there is search entry
  //This will happen after search button will be clicked
  if (isset($_POST['search'])) {
    $field = $_POST['searchfield'];
    $sql = "select * from employeedetails where firstname LIKE '%$field%' OR lastname LIKE '%$field%' OR EmailID LIKE '%$field%'OR salary='$field'OR department LIKE '%$field%'OR gender LIKE '$field%'OR phonenumber LIKE '%$field%'OR employeeid='$field' limit $start,$per_page";
  } else {
    //To show all the records in the database
    //this will happen when add employee button will be clicked
    $sql = "select * from employeedetails limit $start,$per_page";
  }
  $query = mysqli_query($conn, $sql);
  $num = mysqli_num_rows($query);
 //If no employee found
  if ($num == 0) {
    header('location:welcome2.php?error1=1');
  }
  ?>
  <!-- html starting -->
  <!DOCTYPE html>
  <html>
  <head>
    <title>TablePage</title>
    <!-- including header file for libraries -->
    <?php include_once('header.html'); ?>
    <!-- including css file -->
    <link rel="stylesheet" href="../asset/css/styling.css">
  </head>
  <body>
    <div class="topnav">
      <!-- search container -->
      <div class="search-container">
        <form action="welcome2.php" method="post">
          <input type="text" placeholder="Search" class="form-group" name="searchfield">
          <button name="search" class="glyphicon glyphicon-search">search</button>
        </form>
      </div>
    </div>
    <!-- add button -->
    <a href="add.php">
      <button name="add" class="btn btn-success">ADD Employee</button>
    </a>
    <div class="container">
    <!-- the database table -->
      <table class="table table-striped">
        <thead>
          <tr>
            <th>Employee ID</th>
            <th>Profile Picture</th>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Email ID</th>
            <th>Salary</th>
            <th>Department</th>
            <th>Gender</th>
            <th>Phone Number</th>
            <th>Edit</th>
            <th>Delete</th>
          </tr>
        </thead>
        <tbody>
          <?php
          while ($row = mysqli_fetch_assoc($query)) {
          ?>
            <tr>
              <td><?php echo $row['employeeid']; ?></td>
              <td><?php echo '<img  src="data:image;base64,' . base64_encode($row['profilepicture']) . '"  style="width:100px; height:100px ">'; ?></td>
              <td><?php echo $row['firstname']; ?></td>
              <td><?php echo $row['lastname']; ?></td>
              <td><?php echo $row['EmailID']; ?></td>
              <td><?php echo $row['salary']; ?></td>
              <td><?php echo $row['department']; ?></td>
              <td><?php echo $row['gender']; ?></td>
              <td><?php echo $row['phonenumber']; ?></td>
              <!-- Edit button -->
              <td><a href="edit.php?employeeid=<?php echo $row['employeeid']; ?>">
                  <button class="btn btn-success btn-sm rounded-0" type="button" data-toggle="tooltip" data-placement="top" name="edit" title="Edit"><i class="fa fa-edit"></i></button>
                </a></td>
                <!-- Delete button -->
              <td> <button onClick=deleteconfirm(<?php echo $row['employeeid']; ?>) name="delete" class="btn btn-danger btn-sm rounded-0" type="button" data-toggle="tooltip" data-placement="top" title="Delete"><i class="fa fa-trash"></i></button></td>
            </tr>
            <!-- funtion for confirmation of delete -->
            <script>
              function deleteconfirm(id) {
                if (confirm("Do you want to Delete")) {
                  window.location.href = 'actions.php?delete=' + id + '';
                  return true;
                }
              }
            </script>
          <?php
          }
          ?>
        </tbody>
      </table>
      <!-- for pagination -->
      <?php
      $page_query = "select * from employeedetails";
      $page_result = mysqli_query($conn, $page_query);
      $total_result = mysqli_num_rows($page_result);
      $total_page = ceil($total_result / $per_page);
      ?>
      <div class="text-center">
        <?php
        if ($page > 1) {
        ?>
        <!-- previuos button -->
          <a href="welcome2.php?page=<?php echo ($page - 1); ?>">
            <button name="" class="btn btn-success">Previous</button>
          </a>
        <?php
        }
        for ($i = 1; $i <= $total_page; $i++) {
        ?>
        <!-- number of pages -->
          <a href="welcome2.php?page=<?php echo $i; ?>">
            <button name="" class="btn btn-primary"><?php echo $i; ?></button>
          </a>
        <?php
        }
        $i--;
        if ($i > $page) {
        ?>
        <!-- for next button -->
          <a href="welcome2.php?page=<?php echo ($page + 1); ?>">
            <button name="" class="btn btn-success">Next</button>
          </a>
        <?php
        }
        ?>
      </div>
    </div>
    <?php
    if (isset($_POST['search'])) {
    ?>
    <!-- To go to all record button -->
      <a href="welcome2.php">
        <button name="all" class="btn btn-success">All Records</button>
      </a>
    <?php
    }
    ?>
    <!-- change password button -->
    <a href="change.php">
      <button name="change" class="btn btn-success">Change Password</button>
    </a>
  </body>
  </html>
<?php
  //logout button
  echo '<a href="logout.php">
    <button name="logout" class="btn btn-success">Logout</button></a>';
} else {
  header('location:main.php');
}
?>