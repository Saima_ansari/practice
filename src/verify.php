<?php
session_start();

//Includes database connection file
include_once('dbconnection.php');
//If login button is clicked
if (isset($_POST['login'])) {
    //UserEntered values
    $username = $_POST['username'];
    $password = md5($_POST['pwd']);
    //Query for getting the row where username and password matches in database
    $query = "select * from login where username=? AND password=?";
    //prepare sql
    $stmt = $con->prepare($query);
    //bind parameters and execute query
    $stmt->execute([$username, $password]);
    //If there is only one entry 
    if ($stmt->rowCount() == 1) {
        //set the session
        $_SESSION['user'] = $username;
        //head to record page
        header("location:welcome2.php");
    } else {
        //if password and username does not match
        header('location:main.php?error=1');
    }
} 



// //If the login button is clicked it will enter this block
// if (isset($_POST['login'])) 
// {
//     //if the username field has some values
//     if (empty(trim($_POST['username']))) 
//     {
//         //If the username is empty or just blank spaces then it will be redirected to main.php with an error message
//         header('location:main.php?empty');
//     } 
//     else 
//     {
//         //Take the data come from user into $username
//         $username = $_POST['username'];
//         $password = md5($_POST['pwd']);
//         //The query for checking the username and password is correct or not
//         $query = "select * from login where username='$username' and password='$password'";
//         //For executing the query
//         $result = mysqli_query($conn, $query);
//         //For getting the num of rows
//         $row = mysqli_num_rows($result);
//         //Check if there is only one entry
//         if ($row == 1) 
//         {
//             $_SESSION['user'] = $username;
//             //redirect to welcome2.php
//             header("location:welcome2.php");
//         } else 
//         {
//             //if not correct for showing error message
//             header('location:main.php?error=1');
//         }
//     }
// }
