<?php
//session started
session_start();
if (isset($_SESSION['user'])) {
?>
  <!DOCTYPE html>
  <html>
  <head>
    <title>AddEmployeePage</title>
    <link rel="stylesheet" href="../asset/Css/styling.css">
    <?php
    include_once 'header.html' ?>
    <script src="../asset/js/validate.js"></script>
    <!-- for from validation -->
    <style>
      .error {
        color: red;
        font-style: italic;
        font-size: 14px;
      }
    </style>
  </head>
  <body>
    <div class="container">
      <h2>Enter the new employees details</h2>
      <form action="actions.php" method="POST" enctype="multipart/form-data" id="addform">
      <div class="form-row">  
      <div class="col">
          <label class="required" for="firstname">First Name:</label>
          <input type="text" class="form-control" name="firstname" placeholder="Enter the First Name" required>
        </div>
        <div class="col">
          <label class="required" for="lastname">Last Name:</label>
          <input type="text" class="form-control" name="lastname" placeholder="Enter the Last Name" required>
        </div>
    </div>
        <div class="form-group">
          <label class="required" for="email">Email ID:</label>
          <input type="email" class="form-control" name="email" placeholder="Enter the Email ID" required>
        </div>
        <div class="form-group">
          <label class="required" for="salary">Salary:</label>
          <input type="text" class="form-control" name="salary" placeholder="Enter the Salary" required>
        </div>
        <div class="form-group">
          <label class="required" for="dept">Department:</label>
          <select name="dept" class="form-control" required>
            <option value="">None</option>
            <option value="HR">HR</option>
            <option value="SALES">SALES</option>
            <option value="IT">IT</option>
            <option value="Developer">Developer</option>
          </select>
        </div>
        <div class="form-group">
          <label class="required" for="phone">Phone Number:</label>
          <input type="text" class="form-control" name="phone" placeholder="Enter the phone number" required>
        </div>
        <div class="form-group">
          <label for="gender">Gender:</label>
          <label class="form-check form-check-inline">
            <input type="radio" name="gender" value="Female">Female
          </label>
          <label class="form-check form-check-inline">
            <input type="radio" name="gender" value="Male">Male
          </label>
          <label class="form-check form-check-inline">
            <input type="radio" name="gender" value="Other">Other
          </label>
        </div>
        <input type="file" name="picture">
        <button type="submit" name="add" class="btn btn-success">Add Employee</button>
      </form>
  </body>
  </html>
<?php
} else {
  header('location:main.php');
}
?>