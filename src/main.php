<!DOCTYPE html>
<html>
<head>
    <title>MainPage</title>
    <link rel="stylesheet" href="../asset/Css/loginpage.css">
    <?php
    include_once 'header.html' ?>
</head>
<body>
    <h1>Employee Management</h1>
    <h2>Login Form</h2>
    <div class="login_form">
        <form action="verify.php" method="post">
            <div class="container">
                <img src="../asset/img/person-man.png" class="rounded-circle" height="200" width="200" style="margin-left: 27px">
            </div>
            <div class="container">
                <!-- username field -->
                <label for="username"><b>Username</b></label>
                <input type="text" placeholder="Enter username" name="username">
                <!-- password field -->
                <label><b>Password</b></label>
                <input type="password" placeholder="Enter password" name="pwd" required>
                <button type="submit" name="login" class="bbtn btn-info">Login</button>
                <?php
                //If username or password is not correct
                if (isset($_GET['error'])) {
                ?>
                    <div class="alert alert-danger">
                        Incorret Username or Password
                    </div> <?php
                        }
                        if (isset($_GET['empty'])) {
                            ?>
                    <div class="alert alert-danger">
                        Please Enter Username
                    </div> <?php
                        }
                            ?>
            </div>
        </form>
    </div>
</body>
</html>
<?php
