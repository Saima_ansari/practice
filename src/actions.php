<?php
//databaseconnection file
include_once 'dbconnection.php';
session_start();
//check if session is on
if (isset($_SESSION['user'])) {
    //For inserting the details in the database
    if (isset($_POST['add'])) {
        //Take the data from user into variables
        $empid = $_POST['empid'];
        $fname = $_POST['firstname'];
        $lname = $_POST['lastname'];
        $email = $_POST['email'];
        $salary = $_POST['salary'];
        $dept = $_POST['dept'];
        $gender = $_POST['gender'];
        $phone = $_POST['phone'];
        //query for inserting the data
        $query = "INSERT INTO employeedetails(firstname, lastname, EmailID, salary, department, gender,phonenumber) VALUES (?,?,?,?,?,?,?)";
        //prepare the query
        $stmt = $con->prepare($query);
        //bind and execute the query
        $stmt->execute([$fname, $lname, $email, $salary, $dept, $gender, $phone]);
        //after the query will redirect to welcome2.php
        header('location:welcome2.php?add');

        //Take the image name from the user
        // $image_name = addslashes($_FILES['picture']['name']);
        // $tmp_name = $_FILES['picture']['tmp_name'];
        // $image = addslashes(file_get_contents($_FILES['picture']['tmp_name']));
        // $img_ex = pathinfo($image_name, PATHINFO_EXTENSION);
        // $img_ex_lc = strtolower($img_ex);
        // $allowed_ex = array("jpg", "jpeg", "png");
        // if (in_array($img_ex_lc, $allowed_ex)) {
        //     $new_img_name = uniqid("IMG", true) . '.' . $img_ex_lc;
        //     $img_upload_path = 'images/' . $new_img_name;
        //     move_uploaded_file($tmp_name, $img_upload_path);
        //     $sql = "INSERT INTO employeedetails(firstname, lastname, EmailID, salary, department, gender, phonenumber,profilepicture) VALUES ('$fname','$lname','$email','$salary','$dept','$gender','$phone','$image')";
        // }
        // mysqli_query($conn, $sql);
    }
    //for editing the details into database
    if (isset($_POST['update'])) {
        //The data came from the user
        $empid = $_POST['empid'];
        $fname = $_POST['firstname'];
        $lname = $_POST['lastname'];
        $email = $_POST['email'];
        $salary = $_POST['salary'];
        $dept = $_POST['dept'];
        $gender = $_POST['gender'];
        $phone = $_POST['phone'];
        $picture = $_POST['photo'];
        //query for updating or editing all the details
        $query = "update employeedetails SET firstname=?,lastname=?,EmailID=?,salary=?,department=?,gender=?,phonenumber=? where employeeid=?";
        //prepare the query
        $stmt = $con->prepare($query);
        //bind and execute the query
        $stmt->execute([$fname, $lname, $email, $salary, $dept, $gender, $phone, $empid]);






        // $image_name=addslashes($_FILES['picture']['name']);
        // $picture_name = addslashes($_FILES['photo']['name']);
        // $tmp_name = $_FILES['photo']['tmp_name'];
        // $picture = addslashes(file_get_contents($_FILES['photo']['tmp_name']));
        // $img_ex = pathinfo($picture_name, PATHINFO_EXTENSION);
        // $img_ex_lc = strtolower($img_ex);
        // $allowed_ex = array("jpg", "jpeg", "png");
        // if (in_array($img_ex_lc, $allowed_ex)) {
        //     $new_img_name = uniqid("IMG", true) . '.' . $img_ex_lc;
        //     $img_upload_path = 'images/' . $new_img_name;
        //     move_uploaded_file($tmp_name, $img_upload_path);

        // }
        // // $sql="update employeedetails SET firstname='$fname',lastname='$lname',EmailID='$email',salary='$salary',department='$dept',gender='$gender',phonenumber='$phone' where employeeid='$empid'";
        // $sql = "update employeedetails SET firstname='$fname',lastname='$lname',EmailID='$email',salary='$salary',department='$dept',gender='$gender',phonenumber='$phone',profilepicture='$picture' where employeeid='$empid'";
        // $res = mysqli_query($conn, $sql);

        //will be redirected to welcome2.php
        header('location:welcome2.php?update=1');
    }
    //For deleting the entry in the database
    if (isset($_GET['delete'])) {
        //the employee id user wants to delete
        $empid = $_GET['delete'];
        //query for deleting the employee
        $query = "delete from employeedetails where employeeid=?";
        //prepare and connect
        $stmt = $con->prepare($query);
        //bind and execute
        $stmt->execute([$empid]);
        //redirect to welcome2.php
        header('location:welcome2.php');
    }
} else {
    //if session is not set
    header('location:main.php');
}
