<?php
include_once 'dbconnection.php';
session_start();
if (isset($_SESSION['user'])) {
  $id = $_GET['employeeid'];
  //For filling the edit form with prefilled data
  $query = "select * from employeedetails where employeeid=?";

  $stmt = $con->prepare($query);
  $stmt->execute([$id]);
  $rows = $stmt->fetchAll();
  foreach ($rows as $row) {
    $empid = $row['employeeid'];
    $fname = $row['firstname'];
    $lname = $row['lastname'];
    $email = $row['EmailID'];
    $salary = $row['salary'];
    $dept = $row['department'];
    $gender = $row['gender'];
    $phone = $row['phonenumber'];
    $photo = $row['profilepicture'];
  }





  // while ($row = mysqli_fetch_assoc($result)) {
  //   $empid = $row['employeeid'];
  //   $fname = $row['firstname'];
  //   $lname = $row['lastname'];
  //   $email = $row['EmailID'];
  //   $salary = $row['salary'];
  //   $dept = $row['department'];
  //   $gender = $row['gender'];
  //   $phone = $row['phonenumber'];
  //   $photo = $row['profilepicture'];
  // }
?>
  <!-- Edit form -->
  <!DOCTYPE html>
  <html>

  <head>
    <title>EditPage</title>
    <link rel="stylesheet" href="../asset/Css/styling.css">
    <!-- including the header file for all the libraries -->
    <?php include_once('header.html'); ?>
  </head>

  <body>
    <div class="container">
      <h2>Enter the employee details to be updated </h2>
      <form action="actions.php" method="POST" enctype="multipart/form-data">
        <div class="form-group">

          <input type="hidden" class="form-control" name="empid" value="<?php echo $empid; ?>">
        </div>
        <div class="form-group">
          <label class="required" for="firstname">FirstName:</label>
          <input type="text" class="form-control" name="firstname" placeholder="Enter the Firstname" value="<?php echo $fname; ?>">
        </div>
        <div class="form-group">
          <label class="required" for="lastname">LastName:</label>
          <input type="text" class="form-control" name="lastname" placeholder="Enter the laststname" value="<?php echo $lname; ?>">
        </div>
        <div class="form-group">
          <label class="required" for="email">Email ID:</label>
          <input type="text" class="form-control" name="email" placeholder="Enter the Email ID" value="<?php echo $email; ?>">
        </div>
        <div class="form-group">
          <label class="required" for="salary">Salary:</label>
          <input type="text" class="form-control" name="salary" placeholder="Enter the salary" value="<?php echo $salary; ?>">
        </div>
        <div class="form-group">
          <label class="required" for="dept">Department:</label>
          <select name="dept" class="form-control" value="<?php echo $dept ?>">
            <option selected><?php echo $dept ?></option>
            <option value="HR">HR</option>
            <option value="SALES">SALES</option>
            <option value="IT">IT</option>
            <option value="Developer">Developer</option>
          </select>
          <div class="form-group">
            <label class="required" for="phone">Phone Number:</label>
            <input type="text" class="form-control" name="phone" placeholder="Enter the phone number" value="<?php echo $phone; ?>">
          </div>
          <div class="form-group">
            <label class="required" for="gender">Gender:</label>
            <label class="radio-inline">
              <input type="radio" name="gender" value="Female" <?php
                                                                if ($gender == 'Female') {
                                                                  echo "checked";
                                                                }

                                                                ?>>Female
            </label>
            <label class="radio-inline">
              <input type="radio" name="gender" value="Male" <?php
                                                              if ($gender == 'Male') {
                                                                echo "checked";
                                                              }

                                                              ?>>Male
            </label>
            <label class="radio-inline">
              <input type="radio" name="gender" value="Other" <?php
                                                              if ($gender == 'Other') {
                                                                echo "checked";
                                                              }

                                                              ?>>Other
            </label>
          
          </div>
          <div class="form-group">
            <label for="file">Profile Picture</label>
            <?php echo '<img  src="data:image;base64,' . base64_encode($photo) . '"  style="width:100px; height:100px ">'; ?>
            <input type="file" class="form-control" name="photo" placeholder="Choose the file">
          </div>
          <button type="submit" name="update" class="btn btn-success">UPDATE</button>
      </form>
  </body>

  </html>
<?php
} else {
  header('location:main.php');
}
?>