<?php
session_start();
include_once 'dbconnection.php';
if (isset($_SESSION['user'])) {
    if (isset($_POST['change'])) {
        $old_password = md5($_POST['oldpwd']);
        if (strlen($_POST['newpwd']) > 8) {
            $new_password = md5($_POST['newpwd']);
            $confirm_password = md5($_POST['conpwd']);
            $user = $_SESSION['user'];
            if ($new_password == $confirm_password) {
                $sql = "update login set password='$confirm_password' where password='$old_password' and username='$user' ";
                mysqli_query($conn, $sql);
                header('location:welcome2.php?change');
            } else {
                header('location:change.php?error=1');
            }
        } else {
            header('location:change.php?length');
        }
    }
?>
    <!DOCTYPE html>
    <html>
    <head>
        <title>ChangePassword</title>
        <!-- including header file for all the links and libararies -->
       <?php include_once('header.html');?>
        <link rel="stylesheet" href="../asset/Css/changepassword.css">
        
        <style>

        </style>

    </head>

    <body>
        <h2>Change password</h2>
        <div class="login_form">
            <form action="change.php" method="post">

                <div class="container">
                    <label for="oldpwd"><b>Old Password</b></label>
                    <input type="password" placeholder="Enter Old Password" name="oldpwd" required>
                    <label><b>New Password</b></label>
                    <input type="password" placeholder="Enter New Password" name="newpwd" required>
                    <label><b>Confirm Password</b></label>
                    <input type="password" placeholder="Confirm New Password" name="conpwd" required>
                    <button type="submit" name="change" class="bbtn btn-info">Change Password</button>

                    <?php

                    //If username or password is not correct
                    if (isset($_GET['error'])) {
                    ?>
                        <div class="alert alert-danger">
                            Password does not match
                        </div> <?php
                            }

                            if (isset($_GET['length'])) {
                                ?>
                        <div class="alert alert-danger">
                            Password must be atleast 8 characters
                        </div> <?php
                            }




                                ?>



                </div>

            </form>
        </div>

    </body>




    </html>
<?php
} else {

    header('location:main.php');
}
?>