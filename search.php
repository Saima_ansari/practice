<?php
include_once('dbconnection.php');

if (isset($_POST['search'])) {
  $field = $_POST['searchfield'];

  if (isset($_GET['page'])) {
    $page = $_GET['page'];
  } else {
    $page = 1;
  }
  $per_page = 05;
  $start = ($page - 1) * $per_page;
  $field = $_POST['searchfield'];
  $sql = "select * from employeedetails where firstname LIKE '%$field%' OR lastname LIKE '%$field%' OR EmailID LIKE '%$field%'OR salary='$field'OR department LIKE '%$field%'OR gender LIKE '$field%'OR phonenumber LIKE '%$field%'OR employeeid='$field' limit $start,$per_page ";

  $query = mysqli_query($conn, $sql);
  $num = mysqli_num_rows($query);
  if ($num == 0) {
    header('location:welcome2.php?error1=1');
  }

?>


  <!DOCTYPE html>
  <html>

  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <style>
      .topnav .search-container {
        float: right;
      }

      .topnav input[type=text] {
        padding: 6px;
        margin-top: 6px;
        font-size: 17px;
        margin-right: 2px;
      }

      .topnav .search-container button {
        float: right;
        padding: 6px 10px;
        margin-top: 8px;
        margin-right: 13px;
        font-size: 17px;
        border: none;
        cursor: pointer;
      }

      .topnav .search-container button:hover {
        background: #ccc;
      }

      .topnav input[type=text] {
        border: 2px solid #ccc;
      }
    </style>




  </head>

  <body>
    <div class="topnav">


      <div class="search-container">
        <form action="search.php" method="post">
          <input type="text" placeholder="Search" class="form-group" name="searchfield">
          <button name="search" class="btn btn-success">Search</button>
        </form>
      </div>
    </div>
    <!-- Add button -->
    <a href="add.php">
      <button name="add" class="btn btn-success">ADD Employee</button>
    </a>
    <div class="container">

      <!-- the database table -->

      <table class="table table-bordered">
        <thead>
          <tr>
            <th>EmployeeID</th>
            <th>Firstname</th>
            <th>Lastname</th>
            <th>EmailID</th>
            <th>Salary</th>
            <th>Department</th>
            <th>Gender</th>
            <th>PhoneNumber</th>
            <th>Edit</th>
            <th>Delete</th>

          </tr>
        </thead>
        <tbody>
          <?php
          while ($row = mysqli_fetch_assoc($query)) {
          ?>
            <tr>
              <td><?php echo $row['employeeid']; ?></td>
              <td><?php echo $row['firstname']; ?></td>
              <td><?php echo $row['lastname']; ?></td>
              <td><?php echo $row['EmailID']; ?></td>
              <td><?php echo $row['salary']; ?></td>
              <td><?php echo $row['department']; ?></td>
              <td><?php echo $row['gender']; ?></td>
              <td><?php echo $row['phonenumber']; ?></td>
              <!-- Edit button -->
              <td>
                <a href="edit.php?employeeid=<?php echo $row['employeeid']; ?>">
                  <button name="edit" class="btn btn-success">EDIT</button>
                </a>
              </td>

              <td>
                <button onClick=deleteconfirm(<?php echo $row['employeeid']; ?>) name="delete" class="btn btn-danger">DELETE</button>
              </td>
            </tr>
            <script>
              function deleteconfirm(id) {
                if (confirm("Do you want to Delete")) {
                  window.location.href = 'actions.php?delete=' + id + '';
                  return true;

                }
              }
            </script>
        <?php

          }
        }

        ?>
        </tbody>
      </table>
      <?php

      $page_query = "select * from employeedetails where firstname LIKE '%$field%' OR lastname LIKE '%$field%' OR EmailID LIKE '%$field%'OR salary='$field'OR department LIKE '%$field%'OR gender LIKE '$field%'OR phonenumber LIKE '%$field%'OR employeeid='$field'";
      $page_result = mysqli_query($conn, $page_query);
      $total_result = mysqli_num_rows($page_result);
      $total_page = ceil($total_result / $per_page);
      if ($page > 1) {
      ?>
        <a href="search.php?page=<?php echo ($page - 1); ?>?searchfield=<?php echo $field; ?>">
          <button name="" class="btn btn-success">Previous</button>
        </a>
      <?php
      }
      for ($i = 1; $i <= $total_page; $i++) {
      ?>
        <a href="search.php?page=<?php echo $i; ?>?searchfield=<?php echo $field; ?>">
          <button name="" class="btn btn-primary"><?php echo $i; ?></button>

        </a>
      <?php
      }
      $i--;
      if ($i > $page) {

      ?>

        <a href="search.php?page=<?php echo ($page + 1); ?>">
          <button name="" class="btn btn-success">Next</button>

        </a>
      <?php
      }


      ?>