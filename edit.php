<?php
include_once 'dbconnection.php';
session_start();
if (isset($_SESSION['user'])) {


  $id = $_GET['employeeid'];
  $sql = "select * from employeedetails where employeeid='$id'";
  $result = mysqli_query($conn, $sql);
  while ($row = mysqli_fetch_assoc($result)) {

    $empid = $row['employeeid'];
    $fname = $row['firstname'];
    $lname = $row['lastname'];
    $email = $row['EmailID'];
    $salary = $row['salary'];
    $dept = $row['department'];
    $gender = $row['gender'];
    $phone = $row['phonenumber'];
    $photo = $row['profilepicture'];
  }
?>
  <!DOCTYPE html>
  <html>

  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
  </head>

  <body>
    <div class="container">
      <h2>Enter the employee details to be updated </h2>
      <form action="action.php" method="POST" enctype="multipart/form-data">
        <div class="form-group">
          <label for="empid">EmployeeID:</label>
          <input type="text" class="form-control" name="empid" value="<?php echo $empid; ?>">
        </div>
        <div class="form-group">
          <label for="firstname">FirstName:</label>
          <input type="text" class="form-control" name="firstname" placeholder="Enter the Firstname" value="<?php echo $fname; ?>">
        </div>
        <div class="form-group">
          <label for="lastname">LastName:</label>
          <input type="text" class="form-control" name="lastname" placeholder="Enter the laststname" value="<?php echo $lname; ?>">
        </div>
        <div class="form-group">
          <label for="email">Email ID:</label>
          <input type="text" class="form-control" name="email" placeholder="Enter the Email ID" value="<?php echo $email; ?>">
        </div>
        <div class="form-group">
          <label for="salary">Salary:</label>
          <input type="text" class="form-control" name="salary" placeholder="Enter the salary" value="<?php echo $salary; ?>">
        </div>
        <!-- <div class="form-group">
      <label for="dept">Department:</label>
      <input type="text" class="form-control" name="dept" placeholder="Enter the department" value="<?php echo $dept; ?>">
    </div> -->
        <!-- <div class="form-group">
      <label for="gender">Gender:</label>
      <input type="text" class="form-control" name="gender" placeholder="Enter the gender" value="<?php echo $empid; ?>>
    </div> -->
        <div class="form-group">
          <label for="dept">Department:</label>

          <select name="dept" class="form-control" value="<?php echo $dept ?>">
            <option selected><?php echo $dept ?></option>
            <option value="HR">HR</option>
            <option value="SALES">SALES</option>
            <option value="IT">IT</option>
            <option value="Developer">Developer</option>
          </select>
          <div class="form-group">
            <label for="phone">Phone Number:</label>
            <input type="text" class="form-control" name="phone" placeholder="Enter the phone number" value="<?php echo $phone; ?>">
          </div>

          <div class="form-group">
            <label for="gender">Gender:</label>
            <label class="radio-inline">
              <input type="radio" name="gender" value="Female" <?php
                                                                if ($gender == 'Female') {
                                                                  echo "checked";
                                                                }

                                                                ?>>Female
            </label>
            <label class="radio-inline">
              <input type="radio" name="gender" value="Male" <?php
                                                              if ($gender == 'Male') {
                                                                echo "checked";
                                                              }

                                                              ?>>Male
            </label>



          </div>
          <div class="form-group">
            <label for="file">Profile Picture</label>
            <?php echo '<img  src="data:image;base64,' . base64_encode($photo) . '"  style="width:100px; height:100px ">'; ?>

            <input type="file" class="form-control" name="photo" placeholder="Choose the file">
          </div>

          <button type="submit" name="update" class="btn btn-success">UPDATE</button>




      </form>







  </body>


  </html>

<?php
} else {
  header('location:main.php');
}
?>