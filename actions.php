<?php


include_once 'dbconnection.php';
session_start();
if (isset($_SESSION['user'])) {
    include_once 'dbconnection.php';

    //For inserting the details in the database
    if (isset($_POST['add'])) {
        $empid = $_POST['empid'];
        $fname = $_POST['firstname'];
        $lname = $_POST['lastname'];
        $email = $_POST['email'];
        $salary = $_POST['salary'];
        $dept = $_POST['dept'];
        $gender = $_POST['gender'];
        $phone = $_POST['phone'];
        $image_name = addslashes($_FILES['picture']['name']);
        $tmp_name = $_FILES['picture']['tmp_name'];
        $image = addslashes(file_get_contents($_FILES['picture']['tmp_name']));
        $img_ex = pathinfo($image_name, PATHINFO_EXTENSION);
        $img_ex_lc = strtolower($img_ex);
        $allowed_ex = array("jpg", "jpeg", "png");
        if (in_array($img_ex_lc, $allowed_ex)) {
            $new_img_name = uniqid("IMG", true) . '.' . $img_ex_lc;
            $img_upload_path = 'images/' . $new_img_name;
            move_uploaded_file($tmp_name, $img_upload_path);

            //$sql="INSERT INTO employeedetails(employeeid, firstname, lastname, EmailID, salary, department, gender, phonenumber) VALUES ('$empid','$fname','$lname','$email','$salary','$dept','$gender','$phone')";
            $sql = "INSERT INTO employeedetails(firstname, lastname, EmailID, salary, department, gender, phonenumber,profilepicture) VALUES ('$fname','$lname','$email','$salary','$dept','$gender','$phone','$image')";
        }
        mysqli_query($conn, $sql);


        header('location:welcome2.php?add');
    }



    //for editing the details into database
    if (isset($_POST['update'])) {
        $empid = $_POST['empid'];
        $fname = $_POST['firstname'];
        $lname = $_POST['lastname'];
        $email = $_POST['email'];
        $salary = $_POST['salary'];
        $dept = $_POST['dept'];
        $gender = $_POST['gender'];
        $phone = $_POST['phone'];

        // $image_name=addslashes($_FILES['picture']['name']);
        $picture_name = addslashes($_FILES['photo']['name']);
        $tmp_name = $_FILES['photo']['tmp_name'];
        $picture = addslashes(file_get_contents($_FILES['photo']['tmp_name']));

        $img_ex = pathinfo($picture_name, PATHINFO_EXTENSION);
        $img_ex_lc = strtolower($img_ex);
        $allowed_ex = array("jpg", "jpeg", "png");

        if (in_array($img_ex_lc, $allowed_ex)) {
            $new_img_name = uniqid("IMG", true) . '.' . $img_ex_lc;
            $img_upload_path = 'images/' . $new_img_name;
            move_uploaded_file($tmp_name, $img_upload_path);
            $sql = "update employeedetails SET firstname='$fname',lastname='$lname',EmailID='$email',salary='$salary',department='$dept',gender='$gender',phonenumber='$phone',profilepicture='$picture' where employeeid='$empid'";
        }

        // $sql="update employeedetails SET firstname='$fname',lastname='$lname',EmailID='$email',salary='$salary',department='$dept',gender='$gender',phonenumber='$phone' where employeeid='$empid'";
        $res = mysqli_query($conn, $sql);





        header('location:welcome2.php?update=1');
    }


    //For deleting the entry in the database
    if (isset($_GET['delete'])) {
        $empid = $_GET['delete'];
        $sql = "delete from employeedetails where employeeid='$empid'";
        mysqli_query($conn, $sql);

        header('location:welcome2.php');
    }
} else {
    header('location:main.php');
}
