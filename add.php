<?php
include_once 'dbconnection.php';
session_start();
if (isset($_SESSION['user'])) {
?>
  <!DOCTYPE html>
  <html>

  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
  </head>

  <body>
    <div class="container">
      <h2>Enter the employee details</h2>
      <form action="action.php" method="POST" enctype="multipart/form-data">
        <!-- <div class="form-group">
      <label for="empid">EmployeeID:</label>
      <input type="text" class="form-control" name="empid" placeholder="Enter the employeeID" required>
    </div> -->
        <div class="form-group">
          <label for="firstname">FirstName:</label>
          <input type="text" class="form-control" name="firstname" placeholder="Enter the Firstname" required>
        </div>
        <div class="form-group">
          <label for="lastname">LastName:</label>
          <input type="text" class="form-control" name="lastname" placeholder="Enter the laststname" required>
        </div>
        <div class="form-group">
          <label for="email">Email ID:</label>
          <input type="email" class="form-control" name="email" placeholder="Enter the Email ID" required>
        </div>
        <div class="form-group">
          <label for="salary">Salary:</label>
          <input type="text" class="form-control" name="salary" placeholder="Enter the salary" required>
        </div>
        <!-- <div class="form-group">
      <label for="dept">Department:</label>
      <input type="text" class="form-control" name="dept" placeholder="Enter the department">
    </div> -->
        <!-- <div class="form-group">
      <label for="gender">Gender:</label>
      <input type="text" class="form-control" name="gender" placeholder="Enter the gender">
    </div> -->
        <div class="form-group">
          <label for="dept">Department:</label>

          <select name="dept" class="form-control" required>
            <option value="">None</option>
            <option value="HR">HR</option>
            <option value="SALES">SALES</option>
            <option value="IT">IT</option>
            <option value="Developer">Developer</option>
          </select>
        </div>
        <div class="form-group">
          <label for="phone">Phone Number:</label>
          <input type="text" class="form-control" name="phone" placeholder="Enter the phone number" required>
        </div>
        <div class="form-group">
          <label for="gender">Gender:</label>
          <label class="radio-inline">
            <input type="radio" name="gender" value="Female">Female
          </label>
          <label class="radio-inline">
            <input type="radio" name="gender" value="Male">Male
          </label>
        </div>

        <input type="file" name="picture">
        <button type="submit" name="add" class="btn btn-success">Submit</button>


      </form>



  </body>


  </html>

<?php
} else {
  header('location:main.php');
}
?>