<?php

session_start();
include_once 'dbconnection.php';

if (isset($_SESSION['user'])) {

    if (isset($_POST['change'])) {
        $old_password = md5($_POST['oldpwd']);
        $new_password = md5($_POST['newpwd']);
        $confirm_password = md5($_POST['conpwd']);

        $user = $_SESSION['user'];
        if ($new_password == $confirm_password) {
            $sql = "update login set password='$confirm_password' where password='$old_password' and username='$user' ";
            mysqli_query($conn, $sql);

            header('location:welcome2.php?change');
        } else {
            header('location:change.php?error=1');
        }
    }














?>
    <!DOCTYPE html>
    <html>



    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
        <style>
            body {
                font-family: Arial, Helvetica, sans-serif;
            }

            form {
                border: 3px solid #f1f1f1;
            }

            input[type=text],
            input[type=password] {
                width: 100%;
                padding: 12px 20px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                box-sizing: border-box;
            }

            .container {
                padding: 16px;
            }

            .login_form {
                width: 350px;
                margin: 0 auto;
                padding: 25px;
            }

            .button {
                font-size: 25px;
                color: black;
                background-color: rgb(85, 121, 19);
                height: 57px;
                width: 76px;
            }

            h2,
            h1 {
                text-align: center;
                margin-top: 35px;

            }

            label,
            h2,
            h1 {
                color: darkcyan;
            }

            #error1 {

                text-align: centre;
                color: red;
            }
        </style>

    </head>

    <body>
        <h2>Change password</h2>
        <div class="login_form">
            <form action="change.php" method="post">

                <div class="container">
                    <label for="oldpwd"><b>Old Password</b></label>
                    <input type="password" placeholder="Enter Old Password" name="oldpwd" required>
                    <label><b>New Password</b></label>
                    <input type="password" placeholder="Enter New Password" name="newpwd" required>
                    <label><b>Confirm Password</b></label>
                    <input type="password" placeholder="Confirm New Password" name="conpwd" required>
                    <button type="submit" name="change" class="bbtn btn-info">Change Password</button>

                    <?php

                    //If username or password is not correct
                    if (isset($_GET['error'])) {
                    ?>
                        <div class="alert alert-danger">
                            Password does not match
                        </div> <?php
                            }
                                ?>



                </div>

            </form>
        </div>

    </body>




    </html>
<?php
} else {

    header('location:main.php');
}
?>