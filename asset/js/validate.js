$(document).ready(function()
{
    var $addform=$("#addform");
    if($addform.length)
    {
        $addform.validate(
            {
                rules:{
                    firstname:{
                        required:true
                    },
                    lastname:{
                        required:true
                    },
                    email:{
                        required:true,
                        email:true
                    },
                    salary:{
                        required:true,
                        number:true,
                        maxlength:15
                    },
                    phone:{
                        required:true,
                        number:true,
                        minlength:10,
                        maxlength:10,
                    },
                    dept:{
                        required:true

                    }

                },
                messages:{
                    firstname:{
                        required:'Please enter First Name'
                    },
                    lastname:{
                        required:'Please enter Last Name'
                    },
                    email:{
                        required:'Please enter Email ID',
                        email:'The Email should be in format:abc@domain.com'
                    },
                    salary:{
                        required:'Please enter the Salary',
                        maxlength:'salary must be till range of 15 digits only'
                    },
                    phone:{
                        required:'Please enter the Phone Number'
                    },
                    dept:{
                        required:'Please Select the Department'
                    }
                  
                }
            }
        )
    }



})
